# -*- coding: utf-8 -*-
"""
Created on Thu May 21 17:16:21 2020

@author: sriraa
"""
import numpy as np
# This class allows the user to search over a board for any given word.
class WordSearcher:
    #------------------------------PRIVATE-----------------------------------------#
    
    # Takes in a reference to a word search grid and its dimensions
    def __init__(self, grid, rows, cols):
        # A 2d numpy array of characters
        self.__board = grid
        # Stores the dimensions of the board for ease of use
        self.__num_rows = rows
        self.__num_cols = cols
    
    # Returns all the strings formed from the verticals, horizontals, and diagonals
    # no more than dist characters long starting at the given position on board.
    # Also returns the endpoint of each formed string.
    def __get_strings_from_pos(self,r,c,dist):
        # data is stored in form of [string, [end_row,end_column]].
        # the -1 and +1s are to adjust for the length of the string being added/subtracted.
        hor = ['',[r,c-1]]
        ver = ['',[r-1,c]]
        rev_hor = ['',[r,c+1]]
        rev_ver = ['',[r+1,c]]
        # diagonals in form of <r><c>_diag: d = down, u = up, r = right, l = left
        dr_diag = ['',[r-1,c-1]]
        ul_diag = ['',[r+1,c+1]]
        dl_diag = ['',[r-1,c+1]]
        ur_diag = ['',[r+1,c-1]]
        # the strings should be at most the target word's length
        for i in range(0,dist):
            # boundary checks to prevent over/underflows
            d_bound = r+i >= self.__num_rows
            u_bound = r-i < 0
            r_bound = c+i >= self.__num_cols
            l_bound = c-i < 0
            # get the horizontal/vertical strings/end positions
            if not r_bound:
                hor[0] += self.__board[r,c+i]
                hor[1][1] += 1
            if not l_bound:
                rev_hor[0] += self.__board[r,c-i]
                rev_hor[1][1] -= 1
            if not d_bound:
                ver[0] += self.__board[r+i,c]
                ver[1][0] += 1
            if not u_bound:
                rev_ver[0] += self.__board[r-i,c]
                rev_ver[1][0] -= 1
            # get the diagonal strings/end positions
            if not (d_bound or r_bound):
                dr_diag[0] += self.__board[r+i,c+i]
                dr_diag[1][0] += 1
                dr_diag[1][1] += 1
            if not (u_bound or l_bound):
                ul_diag[0] += self.__board[r-i,c-i]
                ul_diag[1][0] -= 1
                ul_diag[1][1] -= 1
            if not (d_bound or l_bound):
                dl_diag[0] += self.__board[r+i,c-i]
                dl_diag[1][0] += 1
                dl_diag[1][1] -= 1
            if not (u_bound or r_bound):
                ur_diag[0] += self.__board[r-i,c+i]
                ur_diag[1][0] -= 1
                ur_diag[1][1] += 1
            
        return [hor, ver, rev_hor, rev_ver, dr_diag, ul_diag, dl_diag, ur_diag]
    
    # formats the given indexes into the desired output
    def __format_out(self, start, end, word):
        print(word + " {}:{} {}:{}".format(start[0],start[1],end[0],end[1]))
    
    
    #------------------------------PUBLIC-----------------------------------------#
    
    # Finds the word in __board and prints out its start/end positions.
    def search(self, word):
        # removes any spaces in the word
        search_word = word.replace(' ','')
        # this may be called a lot, so just create an instance for it
        word_len = len(search_word)
        for r in range(self.__num_rows):
            for c in range(self.__num_cols):
                strs = self.__get_strings_from_pos(r,c, word_len)
                for item in strs:
                    # Print the start/end index of the string if it matches
                    if item[0].upper() == search_word.upper():
                        self.__format_out([r,c],item[1], word)
                        return
        # If the word is not found, return invalid coordinates to indicate so
        self.__format_out([-1,-1],[-1,-1], word)



# Testing the class/search function
if __name__ == "__main__":
    grid = np.array([['h','a','s','d','f'], ['g','e','y','b','h'], ['j','k','l','z','x'],
                     ['c','v','b','l','n'], ['g','o','o','d','o']])
    print(grid)
    # Given test cases (and reversed), 8 directions of a word and boundary tests.
    # Some words have random spaces in between letters to cover for requirements.
    # Some invalid cases were also included (not found, too long of a word)
    words = ['GOOD','DOOG','HELLO','OLLEH','BYE','EYB','LLO','LB O','LVG',' LK J','L E H',
             'LYS','LBF','LZX','XB','AYZN','NZYA','BRUH','JFLKADJFALKDJFLKAJ']
    ws = WordSearcher(grid,5,5)
    for i in range(len(words)):
        ws.search(words[i])