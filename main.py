# -*- coding: utf-8 -*-
"""
Created on Thu May 21 20:47:44 2020

@author: sriraa
"""

from wordsearcher import WordSearcher
import numpy as np

# Allows the user to input as many files as they want
while(True):
    fname = input('Type in filename or type q to quit: ')
    if fname == 'q':
        break
    print()
    # try-catch to avoid crashing
    try:
        file = open(fname)
    except IOError:
        print('Invalid filename:',fname)
        file.close()
        continue
    
    raw = file.read().split('\n')
    file.close()
    # Get/process the dimensions of the board
    dims = raw[0].split('x')

    rows = int(dims[0])
    cols = int(dims[1])
    # Get the board and convert it to a 2d numpy array (easier indexing/faster)
    board = np.array([line.split() for line in raw[1:rows+1]])
    # Get all the words that will be searched
    words = raw[rows+1:]

    # Form our WordSearcher and call it on all the words
    ws = WordSearcher(board,rows,cols)
    for word in words:
        ws.search(word)