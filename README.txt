Submitter: Aayush Sriram
Language: Python
runtime/package manager: PIP

This program reads the input file's name in via console input.
It will also prompt the user for another file until they decide to quit
(so you don't have to run the entire program again for different input).